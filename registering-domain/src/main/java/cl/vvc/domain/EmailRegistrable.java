package cl.vvc.domain;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emails")
public class EmailRegistrable extends Registrable {

	@Id
	@GeneratedValue
	private Integer id;
	private Long userId;
	private String emailFrom;
	private String emailTo;
	private String emailBody;

	public EmailRegistrable() {
		super();
	}

	public EmailRegistrable(Date createdOn, Long userId, String from, String to, String body) {
		super(createdOn);
		this.userId = userId;
		this.emailFrom = from;
		this.emailTo = to;
		this.emailBody = body;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		EmailRegistrable that = (EmailRegistrable) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(userId, that.userId) &&
				Objects.equals(emailFrom, that.emailFrom) &&
				Objects.equals(emailTo, that.emailTo) &&
				Objects.equals(emailBody, that.emailBody);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), id, userId, emailFrom, emailTo, emailBody);
	}

	@Override
	public String toString() {
		return "EmailRegistrable [id=" + id + ", userId=" + userId + ", emailFrom=" + emailFrom + ", emailTo=" + emailTo
				+ ", emailBody=" + emailBody + ", toString()=" + super.toString() + "]";
	}

}
