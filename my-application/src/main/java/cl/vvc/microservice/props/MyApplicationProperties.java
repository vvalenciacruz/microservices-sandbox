package cl.vvc.microservice.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("my-application")
@RefreshScope
@Data
public class MyApplicationProperties {

    private String label1;
    private String label2;

}
