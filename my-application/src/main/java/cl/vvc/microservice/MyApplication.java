package cl.vvc.microservice;

import cl.vvc.microservice.client.user.UserServiceClientFeign;
import cl.vvc.microservice.client.registering.RegisteringServiceClient;
import cl.vvc.microservice.client.sending.SendingServiceClient;
import cl.vvc.microservice.props.MyApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableFeignClients(clients = {UserServiceClientFeign.class, SendingServiceClient.class,
        RegisteringServiceClient.class})
@EnableConfigurationProperties(MyApplicationProperties.class)
public class MyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class, args);
    }

    @Bean
    public AlwaysSampler defaultSampler() {
        return new AlwaysSampler();
    }

}
