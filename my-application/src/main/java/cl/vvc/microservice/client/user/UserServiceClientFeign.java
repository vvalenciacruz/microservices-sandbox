package cl.vvc.microservice.client.user;

import cl.vvc.model.user.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "user-service", path = "/users", fallback = UserServiceFallback.class)
@Primary
public interface UserServiceClientFeign {

    @GetMapping(path = "/{id}")
    UserInfo find(@PathVariable("id") String id);

    @PostMapping
    UserInfo save(@RequestBody UserInfo userInfo);

    @GetMapping
    List<UserInfo> findAll();
}
