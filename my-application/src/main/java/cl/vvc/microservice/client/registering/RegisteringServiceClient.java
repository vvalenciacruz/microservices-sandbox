package cl.vvc.microservice.client.registering;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.model.registering.EmailWrapper;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "registering-service", path = "/register")
public interface RegisteringServiceClient {

    @PostMapping(path = "/email")
    EmailRegistrable email(@RequestBody EmailWrapper emailWrapper);

}
