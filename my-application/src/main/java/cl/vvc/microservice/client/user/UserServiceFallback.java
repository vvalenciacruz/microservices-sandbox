package cl.vvc.microservice.client.user;

import cl.vvc.model.user.UserInfo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserServiceFallback implements UserServiceClientFeign {

    private List<UserInfo> storedUserInfos = new ArrayList<>(0);

    public void setStoredUserInfos(List<UserInfo> storedUserInfos) {
        this.storedUserInfos = storedUserInfos;
    }

    @Override
    public UserInfo find(String id) {

        return createFakeUser();
    }

    private UserInfo createFakeUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(-1L);
        userInfo.setFirstName("User not found");
        return userInfo;
    }

    @Override
    public UserInfo save(UserInfo userInfo) {
        return null;
    }

    @Override
    public List<UserInfo> findAll() {
        return storedUserInfos;
    }

}
