package cl.vvc.microservice.client.sending;

import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "sending-service", path = "/sending")
public interface SendingServiceClient {

    @PutMapping(path = "/send")
    EmailMessage sendToUser(@RequestBody UserInfo userInfo);

    @GetMapping(path = "/stopSending")
    Boolean stopSending(@RequestParam("userId") Integer userId);
}
