package cl.vvc.microservice.delegate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.microservice.client.registering.RegisteringServiceClient;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.registering.EmailWrapper;
import cl.vvc.model.user.UserInfo;

@Component
public class RegisteringServiceDelegate {

	private final RegisteringServiceClient client;

	@Autowired
	public RegisteringServiceDelegate(RegisteringServiceClient client) {
		this.client = client;
	}

	public EmailRegistrable email(UserInfo userInfo, EmailMessage emailMessage) {
		return client.email(new EmailWrapper(userInfo, emailMessage));
	}

}
