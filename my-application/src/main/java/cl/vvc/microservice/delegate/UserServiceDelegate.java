package cl.vvc.microservice.delegate;

import cl.vvc.microservice.client.user.UserServiceClientFeign;
import cl.vvc.microservice.client.user.UserServiceFallback;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceDelegate {

    private final UserServiceClientFeign client;
    private final UserServiceFallback fallback;

    @Autowired
    public UserServiceDelegate(UserServiceClientFeign client, UserServiceFallback fallback) {
        this.client = client;
        this.fallback = fallback;
    }

    public List<UserInfo> findAll() {
        List<UserInfo> findAll = client.findAll();

        if (!findAll.isEmpty() && findAll.size() > 1) {
            fallback.setStoredUserInfos(findAll);
        }

        return findAll;
    }

}
