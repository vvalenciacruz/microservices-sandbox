package cl.vvc.microservice.delegate;

import cl.vvc.microservice.client.sending.SendingServiceClient;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class SendingServiceDelegate {

    private final SendingServiceClient client;
    private final DiscoveryClient discoveryClient;
    private final RestTemplate restTemplate;

    @Autowired
    public SendingServiceDelegate(SendingServiceClient client, DiscoveryClient discoveryClient) {
        this.client = client;
        this.discoveryClient = discoveryClient;
        this.restTemplate = new RestTemplate();
    }

    public EmailMessage sendToUser(UserInfo userInfo) {
        return client.sendToUser(userInfo);
    }

    public Boolean stopSending(int userId) {
        List<ServiceInstance> instances = discoveryClient.getInstances("sending-service");
        instances.forEach(serviceInstance -> {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<String> entity = new HttpEntity<>(headers);

            String serviceEndpoint = serviceInstance.getUri().toString().concat("/sending/stopSending?userId={userId}");
            restTemplate.exchange(serviceEndpoint, HttpMethod.GET, entity, Boolean.class, userId);

        });

        return Boolean.TRUE;
    }

}
