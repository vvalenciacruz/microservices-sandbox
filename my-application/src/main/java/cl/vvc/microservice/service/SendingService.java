package cl.vvc.microservice.service;

import cl.vvc.microservice.delegate.SendingServiceDelegate;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendingService {

    private final SendingServiceDelegate delegate;

    @Autowired
    public SendingService(SendingServiceDelegate delegate) {
        this.delegate = delegate;
    }

    public EmailMessage sendToUser(UserInfo userInfo) {
        return delegate.sendToUser(userInfo);
    }

    public boolean stopSending(int userId) {
        return delegate.stopSending(userId);
    }

}
