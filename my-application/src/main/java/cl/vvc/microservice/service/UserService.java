package cl.vvc.microservice.service;

import cl.vvc.microservice.delegate.UserServiceDelegate;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserServiceDelegate delegate;

    @Autowired
    public UserService(UserServiceDelegate delegate) {
        this.delegate = delegate;
    }

    public List<UserInfo> findAll() {
        return delegate.findAll();
    }

}
