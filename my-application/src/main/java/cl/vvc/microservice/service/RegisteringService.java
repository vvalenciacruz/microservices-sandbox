package cl.vvc.microservice.service;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.microservice.delegate.RegisteringServiceDelegate;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisteringService {

    private final RegisteringServiceDelegate delegate;

    @Autowired
    public RegisteringService(RegisteringServiceDelegate delegate) {
        this.delegate = delegate;
    }

    public EmailRegistrable email(UserInfo userInfo, EmailMessage emailMessage) {
        return delegate.email(userInfo, emailMessage);
    }

}
