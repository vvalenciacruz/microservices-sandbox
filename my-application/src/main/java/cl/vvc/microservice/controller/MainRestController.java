package cl.vvc.microservice.controller;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.microservice.props.MyApplicationProperties;
import cl.vvc.microservice.service.RegisteringService;
import cl.vvc.microservice.service.SendingService;
import cl.vvc.microservice.service.UserService;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RequestMapping("/api")
@RestController
@Slf4j
public class MainRestController {

    private final UserService userService;
    private final SendingService sendingService;
    private final RegisteringService registeringService;
    private final MyApplicationProperties myApplicationProperties;

    @Autowired
    public MainRestController(UserService userService, SendingService sendingService,
                              RegisteringService registeringService, MyApplicationProperties myApplicationProperties) {
        this.userService = userService;
        this.sendingService = sendingService;
        this.registeringService = registeringService;
        this.myApplicationProperties = myApplicationProperties;
    }

    @GetMapping("/test")
    public String test() {
        log.info("test|Request. void");

        final List<UserInfo> users = userService.findAll();

        final String printableResult;
        if (users.isEmpty()) {
            printableResult = "No results available";
        } else {
            UserInfo firstUser = users.get(0);
            EmailMessage emailMessage = sendingService.sendToUser(firstUser);
            EmailRegistrable emailRegistrable = registeringService.email(firstUser, emailMessage);
            printableResult = createPrintableSendingResult(firstUser, emailMessage, emailRegistrable);
        }

        log.info("test|Response. printableResult={}", printableResult);

        return printableResult;
    }

    private String createPrintableSendingResult(@Nullable UserInfo userInfo, @Nullable EmailMessage emailMessage,
                                                @Nullable EmailRegistrable emailRegistrable) {

        if (userInfo == null && emailMessage == null && emailRegistrable == null) {
            return "No results available";
        }

        StringBuilder out = new StringBuilder();

        if (userInfo != null) {
            out.append(userInfo).append("\n");
        }

        if (emailRegistrable != null) {
            out.append(emailMessage).append("\n")
                    .append(emailRegistrable);
        }

        return out.toString();

    }

    @GetMapping("/load")
    public List<UserInfo> load(@RequestParam(name = "quantity", defaultValue = "10000") int quantity,
                               @RequestParam(name = "waitTimeInMillis", defaultValue = "500") int waitTimeInMillis)
            throws InterruptedException {

        log.info("load|Request. quantity=(), waitTimeInMillis=()", quantity, waitTimeInMillis);

        List<UserInfo> accumulatedUsers = new ArrayList<>();

        for (int i = 0; i < quantity; i++) {
            List<UserInfo> usersInIteration = userService.findAll();
            accumulatedUsers.addAll(usersInIteration);

            log.info("load|Response. index={}, usersInIteration={}", i, usersInIteration);

            TimeUnit.MILLISECONDS.sleep(waitTimeInMillis);
        }

        return accumulatedUsers;
    }

    @GetMapping("/broadcast/stopSending")
    public boolean stopSending(@RequestParam("userId") Integer userId) {
        log.info("stopSending|Request. userId={}", userId);

        boolean result = sendingService.stopSending(userId);

        log.info("stopSending|Response. result={}", result);

        return result;
    }

    @GetMapping("/extProperties")
    public String extProperties() {
        log.info("extProperties|Request. void");

        String result = String.format("This is the value of label1: %s\n"
                + "This is the value of label2: %s", myApplicationProperties.getLabel1(), myApplicationProperties.getLabel2());

        log.info("extProperties|Response. result={}", result);

        return result;
    }

}
