# Microservices Sandbox

[![pipeline status](https://gitlab.com/vvalenciacruz/microservices-sandbox/badges/master/pipeline.svg)](https://gitlab.com/vvalenciacruz/microservices-sandbox/commits/master)
[![coverage report](https://gitlab.com/vvalenciacruz/microservices-sandbox/badges/master/coverage.svg)](https://gitlab.com/vvalenciacruz/microservices-sandbox/commits/master)

Conjunto de microservicios, para explicar y practicar los distintos
patrones de una arquitectura de microservicios.

La intención de la solución completa es realizar envíos masivos de emails a usuarios.

## Microservicios

* user: Repositorio de usuarios.
Expone funcionalidades (CRUD) mediante API REST y almacena datos con JPA.
Tecnologías: Spring Boot. Spring Cloud. JPA.

* sending: Permite enviar emails a usuarios.
Expone funcionalidades mediante API REST.
Tecnologías: Spring Boot. Spring Cloud.

* registering: Permite registrar el envío de emails a un usuario. 
Expone funcionalidades mediante API REST y almacena datos con JPA.
Tecnologías: Spring Boot. Spring Cloud. JPA.

## Microservicios de soporte

* spring-cloud-config-server: Expone configuración almacenada en GIT

* spring-boot-admin: Permite centralizar el monitoreo (simple) de microservicios. 
Funcionalidades principales: Información de runtime, modificar configuración y nivel de logging.

## Aplicación de ejemplo

* my-application: Recorre la base de usuarios, realiza el envío de emails y registra los envíos.
Tecnologías: Spring Boot. Spring Cloud.

## Ambiente

### Local

* Para desarrollar, se necesita agregar/configurar lombok en el IDE (para annotation processing). 
Ver: https://projectlombok.org/