package cl.vvc.microservice.user;

import cl.vvc.microservice.user.domain.UserEntity;
import cl.vvc.microservice.user.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldSaveUserToDbWithTheGivenJsonRequest() throws Exception {

        String jsonRequest = "{\n" +
                "  \"firstName\": \"Juanito\",\n" +
                "  \"lastName\": \"Pérez\",\n" +
                "  \"email\": \"jperez@noop.cl\"\n" +
                "}";

        mvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(not(isEmptyString()))))
                .andExpect(jsonPath("$.firstName", is("Juanito")))
                .andExpect(jsonPath("$.lastName", is("Pérez")))
                .andExpect(jsonPath("$.email", is("jperez@noop.cl")));

    }

    @Test
    public void shouldGetUserFromDbWithTheGivenId() throws Exception {

        UserEntity userEntity = new UserEntity("First", "Last", "a@b.cl");
        userRepository.saveAndFlush(userEntity);

        UserEntity userFromDb = userRepository.findAll().get(0);

        mvc.perform(get("/users/{id}", String.valueOf(userFromDb.getId())))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                // FIXME: value validation of Long doesn't seem to work with JsonPath
                .andExpect(jsonPath("$.firstName", is("First")))
                .andExpect(jsonPath("$.lastName", is("Last")))
                .andExpect(jsonPath("$.email", is("a@b.cl")));

    }

    @Test
    public void shouldGetAllTheUsersInTheDb() throws Exception {

        userRepository.save(Arrays.asList(
                new UserEntity("1", "One", "1@b.cl"),
                new UserEntity("2", "Two", "2@b.cl"),
                new UserEntity("3", "Three", "3@b.cl")
        ));

        mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(3)))
                .andExpect(jsonPath("$.[*].firstName", containsInAnyOrder("1", "2", "3")))
                .andExpect(jsonPath("$.[*].lastName", containsInAnyOrder("One", "Two", "Three")))
                .andExpect(jsonPath("$.[*].email", containsInAnyOrder("1@b.cl", "2@b.cl", "3@b.cl")));

    }

    @Before
    public void cleanUserTable() {
        userRepository.deleteAll();
    }

}
