package cl.vvc.microservice.user.service;

import cl.vvc.microservice.user.repository.UserRepository;
import cl.vvc.microservice.user.util.ObjectConverter;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository repository;
    private final ObjectConverter objectConverter;

    @Autowired
    public UserService(UserRepository repository, ObjectConverter objectConverter) {
        this.repository = repository;
        this.objectConverter = objectConverter;
    }

    public UserInfo find(String id) {
        Long dbId = Long.parseLong(id);
        return objectConverter.convert(repository.findOne(dbId));
    }

    public UserInfo save(UserInfo userInfo) {
        return objectConverter.convert(repository.save(objectConverter.convert(userInfo)));
    }

    public List<UserInfo> findAll() {
        return objectConverter.convert(repository.findAll());
    }

}
