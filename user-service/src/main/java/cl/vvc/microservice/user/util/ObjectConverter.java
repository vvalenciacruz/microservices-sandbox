package cl.vvc.microservice.user.util;

import cl.vvc.microservice.user.domain.UserEntity;
import cl.vvc.model.user.UserInfo;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ObjectConverter {

    private static final Logger logger = LoggerFactory.getLogger(ObjectConverter.class);

    public UserInfo convert(UserEntity userEntity) {
        UserInfo userInfo = new UserInfo();

        try {
            BeanUtils.copyProperties(userInfo, userEntity);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error("convert|message={}", e.getMessage(), e);
        }

        return userInfo;
    }

    public UserEntity convert(UserInfo userInfo) {
        UserEntity userEntity = new UserEntity();
        try {
            BeanUtils.copyProperties(userEntity, userInfo);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error("convert|message={}", e.getMessage(), e);
        }
        return userEntity;
    }

    public List<UserInfo> convert(List<UserEntity> findAll) {
        List<UserInfo> users = new ArrayList<>(findAll.size());
        findAll.forEach(ue -> users.add(convert(ue)));
        return users;
    }

}
