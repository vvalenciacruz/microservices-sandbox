package cl.vvc.microservice.user.controller;

import cl.vvc.microservice.user.service.UserService;
import cl.vvc.model.user.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserRestController {

    private final UserService service;

    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    @GetMapping(path = "/{id}")
    public UserInfo find(@PathVariable("id") String id) {
        log.info("find|Request. id={}", id);

        UserInfo userInfo = service.find(id);

        log.info("find|Response. userInfo={}", userInfo);

        return userInfo;
    }

    @PostMapping
    public UserInfo save(@RequestBody UserInfo userInfo) {
        log.info("save|Request. userInfo={}", userInfo);

        UserInfo savedUserInfo = service.save(userInfo);

        log.info("save|Response. otherUserInfo={}", savedUserInfo);

        return savedUserInfo;
    }

    @GetMapping
    public List<UserInfo> findAll() {
        log.info("save|Request. void");

        List<UserInfo> userInfos = service.findAll();

        log.info("save|Response. userInfos={}", userInfos);

        return userInfos;
    }
}
