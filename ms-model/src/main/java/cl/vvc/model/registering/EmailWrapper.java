package cl.vvc.model.registering;

import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class EmailWrapper {

	private UserInfo userInfo;
	private EmailMessage emailMessage;

}
