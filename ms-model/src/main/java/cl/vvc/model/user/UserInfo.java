package cl.vvc.model.user;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstName;
	private String lastName;
	private String email;

}
