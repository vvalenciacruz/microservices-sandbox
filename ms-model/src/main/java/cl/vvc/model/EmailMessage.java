package cl.vvc.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailMessage {

	private String from;
	private String to;
	private String body;

}
