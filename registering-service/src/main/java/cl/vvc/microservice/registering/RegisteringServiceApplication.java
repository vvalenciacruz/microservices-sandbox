package cl.vvc.microservice.registering;

import cl.vvc.domain.EmailRegistrable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EntityScan(basePackageClasses = EmailRegistrable.class)
@EnableDiscoveryClient
public class RegisteringServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(RegisteringServiceApplication.class, args);
    }

}
