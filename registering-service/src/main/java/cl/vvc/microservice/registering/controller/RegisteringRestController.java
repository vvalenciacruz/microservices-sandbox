package cl.vvc.microservice.registering.controller;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.microservice.registering.service.RegisteringService;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.registering.EmailWrapper;
import cl.vvc.model.user.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
@Slf4j
public class RegisteringRestController {

    private final RegisteringService registeringService;

    @Autowired
    public RegisteringRestController(RegisteringService registeringService) {
        this.registeringService = registeringService;
    }

    @PostMapping("/email")
    public EmailRegistrable email(@RequestBody EmailWrapper emailWrapper) {

        UserInfo userInfo = emailWrapper.getUserInfo();
        EmailMessage emailMessage = emailWrapper.getEmailMessage();

        log.info("Request. userInfo={}, emailMessage={}", userInfo, emailMessage);
        EmailRegistrable registrable = registeringService.email(userInfo, emailMessage);
        log.info("Response. emailRegistrable={}", emailMessage);

        return registrable;
    }

}
