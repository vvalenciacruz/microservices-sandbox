package cl.vvc.microservice.registering.repository;

import cl.vvc.domain.EmailRegistrable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<EmailRegistrable, Integer> {

}
