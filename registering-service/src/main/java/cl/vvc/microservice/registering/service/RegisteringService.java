package cl.vvc.microservice.registering.service;

import cl.vvc.domain.EmailRegistrable;
import cl.vvc.microservice.registering.repository.EmailRepository;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RegisteringService {

    private final EmailRepository emailRepository;

    @Autowired
    public RegisteringService(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    public EmailRegistrable email(UserInfo userInfo, EmailMessage emailMessage) {
        EmailRegistrable emailRegistrable = new EmailRegistrable(new Date(), userInfo.getId(), emailMessage.getFrom(),
                emailMessage.getTo(), emailMessage.getBody());

        return emailRepository.save(emailRegistrable);
    }

}
