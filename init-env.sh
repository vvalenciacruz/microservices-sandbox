#!/bin/bash
echo "Start postgresql"
docker run -d --name postgresql-9_6 -p 5433:5432 -e POSTGRES_PASSWORD=mysecretpassword postgres:9.6-alpine
docker start postgresql-9_6

echo "Starting consul"
docker run -d --name consul -p 8400:8400 -p 8500:8500 -p 8600:53/udp -h node1 progrium/consul -server -bootstrap -ui-dir /ui
docker start consul

echo "Starting Zipkin"
docker run -d --name zipkin -p 9411:9411 openzipkin/zipkin
docker start zipkin

echo "Building microservices"
./gradlew build

echo "Starting microservices"
java -jar spring-boot-admin/build/libs/spring-boot-admin.jar
java -jar spring-cloud-config-server/build/libs/spring-cloud-config-server-0.0.1-SNAPSHOT.jar
java -jar user-service/build/libs/user-service-0.0.1-SNAPSHOT.jar
java -jar registering-service/build/libs/registering-service-0.0.1-SNAPSHOT.jar
java -jar sending-service/build/libs/sending-service-0.0.1-SNAPSHOT.jar
java -jar my-application/build/libs/my-application-0.0.1-SNAPSHOT.jar

echo "URLs"
#Consul http://localhost:8500/
#Spring Boot Admin: http://localhost:7000/
#Zipkin: http://localhost:9411/
#Config Server: http://localhost:8888/

#MyApp: 
#Multi service test: http://localhost:8000/api/test
#Load test: http://localhost:8000/api/load?quantity=10&waitTimeInMillis=100
#Broadcast to all instances: http://localhost:8000/api/broadcast/stopSending?userId=1

#users
#All users: http://localhost:8001/users


echo "Opening Consul in browser"
URL="http://localhost:8500"
if which xdg-open > /dev/null
then
  xdg-open $URL
elif which gnome-open > /dev/null
then
  gnome-open $URL
fi
