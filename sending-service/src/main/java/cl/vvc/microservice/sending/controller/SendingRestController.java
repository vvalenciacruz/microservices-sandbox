package cl.vvc.microservice.sending.controller;

import cl.vvc.microservice.sending.service.MailSendingService;
import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RequestMapping("/sending")
@RestController
@Slf4j
public class SendingRestController {

    private final MailSendingService mailSendingService;
    private final Environment env;

    @Autowired
    public SendingRestController(MailSendingService mailSendingService, Environment env) {
        this.mailSendingService = mailSendingService;
        this.env = env;
    }

    @PutMapping("/send")
    public EmailMessage sendToUser(@RequestBody UserInfo userInfo) {
        log.info("Request. userInfo={}", userInfo);
        EmailMessage message = mailSendingService.send(userInfo);
        log.info("Response. message={}", message);

        return message;
    }

    @GetMapping("/stopSending")
    public Boolean stopSending(@RequestParam("userId") Integer userId) {
        boolean result = true;

        log.info("Request. userId={}", userId);

        try {
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            String port = env.getProperty("server.port");
            log.info("hostAddress={}, port={}", hostAddress, port);
        } catch (UnknownHostException e) {
            log.error("message={}", e.getMessage(), e);
            result = false;
        }

        log.info("Response. result={}", result);

        return result;
    }
}
