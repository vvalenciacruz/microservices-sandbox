package cl.vvc.microservice.sending.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cl.vvc.model.EmailMessage;
import cl.vvc.model.user.UserInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MailSendingService {

	@Value("${mail.from:my@company.com}")
	private String from;

	public EmailMessage send(UserInfo userInfo) {
		String email = userInfo.getEmail();
		String body = new StringBuilder("Dear ").append(userInfo.getFirstName()).append(" ")
				.append(userInfo.getLastName()).append("\n").append("This is a test message.").toString();
		EmailMessage message = new EmailMessage(from, email, body);

		log.info("message={}", message);
		return message;
	}

}
